<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'erahajj_wiki' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '12345' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '?x(<ZDnInt,T|%PW~e[Dt1D+V^vIYXE_ZOxB`*WVX$aI-%5a*IhN>] 4H9MutfRo' );
define( 'SECURE_AUTH_KEY',  's~seHmThRhdcakH(1ux]Tzx*jyXJ?6)raFZt;!6f%#)H`8;x//A*)M7ty?1QYys+' );
define( 'LOGGED_IN_KEY',    '5;n@OeBBEG>WPS%$t=Cth+bieRvb`XVY=?:e>8(2IPF!xfNl8647*-kxNk}7{k]p' );
define( 'NONCE_KEY',        'N*GJHPl5miZjN4*R4ul=Cw`5pnSn~ShUH_n-i5y5.<4AwCv[ZWkiMn9GMdKo0.32' );
define( 'AUTH_SALT',        'aBoPi7Y,sQy`7eF_&c|hOPl1gBK8c6nvkIoY cEDCs47H)KB38cr`:[Ss.~?b8zK' );
define( 'SECURE_AUTH_SALT', 'j-Cg7f,@LuG52fDY:Iv;|Or2v}sKIo8~#Iv!Y<jc8cqKff&g-}pY8+;&K`g7b,j(' );
define( 'LOGGED_IN_SALT',   '^py8EW )m`8}ZEniCX[L]3dDvzIXxVH#3QN/qDCnMpK{&2PI<^[=mUv7X#@|7*.^' );
define( 'NONCE_SALT',       '_4aMf#psTkiu%DX5nY<H![aN.Bhe%G]1:Q;M&{u,pny1K&9hR)etVe;g;s^6DnDq' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
